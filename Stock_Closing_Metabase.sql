SELECT 	
		product_code, 
		product_name, 
		product_category, 
		product_uom, 
		product_taxes, 
		product_barcode, 
		source_location, 
		dest_location, 
		transaction_date, 
		Quantity		
	FROM public."FACT_REPORT_STOCK_CLOSING"
	order by transaction_date
