WITH data as (
	
SELECT
	e.id as id_move,
	a.default_code as Product_Code,
	a.name as Product_Name,
	b.name as Product_Category,
	c.name Product_UOM,
	i.name as Product_Taxes,
	d.barcode as Product_Barcode,
	f.complete_name as Source_Location,
	g.complete_name as Dest_Location,
	e.create_date as Transaction_Date,
	e.qty_done as Quantity,
	case
		when f.usage = 'internal' then 'OUTBOND'
		else 'INBOND'
	end as Transaction_Type
FROM product_template a
LEFT JOIN product_category b on a.categ_id = b.id
LEFT JOIN uom_uom c on a.uom_id = c.id
LEFT JOIN product_product d on a.id = d.product_tmpl_id
LEFT JOIN stock_move_line e on d.id = e.product_id
LEFT JOIN stock_location f on e.location_id = f.id
LEFT JOIN stock_location g on e.location_dest_id = g.id
LEFT JOIN product_taxes_rel h on h.prod_id = a.id
LEFT JOIN account_tax i on h.tax_id = i.id
where a.default_code = '100001'
	and e.state = 'done'
	and a.active = 'true'
--order by e.create_date asc
),
data_convert as(
	select 
    	id_move,
		Product_Code,
      	Product_Name,
		Product_Category,
		Product_UOM,
		Product_Taxes,
		Product_Barcode,
		Source_Location,
		Dest_Location,
      	Transaction_Date,
      	Quantity,
      	case when Transaction_Type='INBOND' then 1*Quantity 
	  		else -1*Quantity 
	  	end AS Convert_Qty,
      	Transaction_Type,
     	LAG(Quantity,1) OVER 
        (
         	PARTITION BY Product_Name
        	ORDER BY id_move
      	) AS Initial_Stock
      
    from data 
	
),
data_end_stock AS (
  select 
     id_move,
		Product_Code,
      	Product_Name,
		Product_Category,
		Product_UOM,
		Product_Taxes,
		Product_Barcode,
		Source_Location,
		Dest_Location,
      	Transaction_Date,
      	Quantity,
		Initial_Stock,
      	sum(Convert_Qty) OVER (PARTITION BY Product_Name
                         ORDER BY id_move) 
						 AS End_Stock,
      Convert_Qty,
	  Transaction_Type
    from data_convert
	--order by id_move asc
	)

SELECT   
     	--id_move,
		Product_Code,
      	Product_Name,
		Product_Category,
		Product_UOM,
		Product_Taxes,
		Product_Barcode,
		Source_Location,
		Dest_Location,
      	Transaction_Date,
      	sum(End_Stock) as Quantity
   from data_end_stock
   group by Product_Code, 
		Product_Name, 
		Product_Category, 
		Product_UOM, 
		Product_Taxes, 
		Product_Barcode, 
		Source_Location, 
		Dest_Location, 
		Transaction_Date
	order by Transaction_Date asc
